import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-info">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          Wardrobify
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/">
                Home
              </NavLink>
            </li>
            <div className="dropdown nav-item">
              <button
                className="btn dropdown-toggle nav-item"
                type="button"
                id="dropdownMenuButton1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Shoes
              </button>
              <ul
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton1"
              >
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/shoes">
                    Shoes
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/shoes/new">
                    Create Shoes
                  </NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown nav-item">
              <button
                className="btn dropdown-toggle nav-item"
                type="button"
                id="dropdownMenuButton1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Hats
              </button>
              <ul
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton1"
              >
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/hats/create">
                    Create a Hat
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/hats/list">
                    Hat's List
                  </NavLink>
                </li>
              </ul>
            </div>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
