import React, { useEffect, useState } from "react";
function HatForm() {
  const [location, setLocation] = useState("");
  const locationChanged = (event) => {
    setLocation(event.target.value);
  };
  const [fabric, setFabric] = useState("");
  const fabricChanged = (event) => {
    setFabric(event.target.value);
  };
  const [style_name, setStyleName] = useState("");
  const styleNameChanged = (event) => {
    setStyleName(event.target.value);
  };
  const [color, setColor] = useState("");
  const colorChanged = (event) => {
    setColor(event.target.value);
  };
  const [picture_url, setPicture] = useState("");
  const pictureChanged = (event) => {
    setPicture(event.target.value);
  };

  const [locations, setLocations] = useState([]);
  const getLocationData = async () => {
    try {
      const response = await fetch("http://localhost:8100/api/locations/");
      const data = await response.json();
      setLocations(data.locations);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    getLocationData();
  }, []);
  const submitForm = async (event) => {
    event.preventDefault();
    const body = {
      fabric,
      style_name,
      colors,
      picture_url,
      location,
    };
    console.log(body);
    const hatUrl = "http://localhost:8090/api/hats/hats_list";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(body),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      console.log(newHat);
      setFabric("");
      setStyleName("");
      setColor("");
      setPicture("");
      setLocation("");
      window.location.reload();
    }
  };
  return (
    <div className="row">
      <div className="offset-3 col-4 m-auto">
        <div className="shadow-lg rounded p-4 mt-5">
          <h2 className="text-center mb-2 text-muted">Create a new Hat</h2>
          <form id="create-location-form" onSubmit={submitForm}>
            <div className="form-floating mb-3">
              <input
                placeholder="Fabric"
                required
                type="text"
                id="fabric"
                className="form-control"
                name="fabric"
                value={fabric}
                onChange={fabricChanged}
              />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Starts"
                required
                type="text"
                id="style_name"
                className="form-control"
                name="style_name"
                value={style_name}
                onChange={styleNameChanged}
              />
              <label htmlFor="Starts">Style Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Color"
                required
                type="text"
                id="color"
                className="form-control"
                name="color"
                value={color}
                onChange={colorChanged}
              />
              <label htmlFor="Color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Picture Url"
                required
                type="url"
                id="picture_url"
                className="form-control"
                name="picture_url"
                value={picture_url}
                onChange={pictureChanged}
              />
              <label htmlFor="picture_url">Picture Url</label>
            </div>
            <div className="mb-3">
              <select
                required
                id="location"
                className="form-select"
                name="location"
                value={location}
                onChange={locationChanged}
              >
                <option>Choose a location</option>
                {locations.map((location) => {
                  return (
                    <option key={location.href} value={location.href}>
                      {location.closet_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="text-center">
              <button className="btn btn-secondary">Create</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
export default HatForm;
