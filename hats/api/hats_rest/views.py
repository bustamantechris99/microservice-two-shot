from django.shortcuts import render
from common.json import ModelEncoder
from .models import LocationVO, Hats
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

class LocationEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "name",
        "href"
    ]

class HatsEncoder(ModelEncoder):
    model = Hats
    properties = [
      "fabric",
      "style_name",
      "color",
      "picture_url",
      "location",
      "id"
    ]
    encoders={
      "location": LocationEncoder()
    }
@require_http_methods(["GET", "POST"])
def get_hats_list(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse({"hats": hats}, encoder=HatsEncoder, safe=False)
    else:
        content = json.loads(request.body)
        try:
          location_href = content["location"]
          location = LocationVO.objects.get(href=location_href)
          content["location"]=location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location"},
                status=400,
            )
        hat = Hats.objects.create(**content)
        return JsonResponse(hat, encoder=HatsEncoder, safe=False)

@require_http_methods(["GET", "DELETE"])
def get_hat(request, id):
    if request.method == "GET":
        try:
          hat = Hats.objects.get(id=id)
          return JsonResponse({"Hat": hat}, HatsEncoder, safe=False)
        except hat.DoesNotExist:
          return JsonResponse({"message": "Hat does not exist!"}, status=400)
    else:
        count, _ = Hats.objects.get(id=id).delete()
        return JsonResponse({"delete": count>0})
