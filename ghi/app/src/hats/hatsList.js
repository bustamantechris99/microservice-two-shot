import React, { useState, useEffect } from "react";
import "../../src/index.css";
export default function HatsList() {
  const [hats, setHats] = useState([]);

  const getHats = async () => {
    const hatsResponse = await fetch(
      "http://localhost:8090/api/hats/hats_list"
    );
    if (hatsResponse.ok) {
      const hats = await hatsResponse.json();
      console.log(hats.hats);
      setHats(hats.hats);
    }
  };
  useEffect(() => {
    getHats();
  }, [hats]);

  console.log(hats);
  const deleteHat = async (id) => {
    const hatUrl = `http://localhost:8090/api/hats/${id}`;
    const fetchConfig = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      console.log("deleted");
      console.log(hats);
      setHats(hats);
    } else {
      console.log("not deleted");
    }
  };
  return (
    <table className="table ">
      <thead>
        <tr>
          <th>Fabric</th>
          <th>Style Name</th>
          <th>Color</th>
          <th>Picture</th>
          <th>Location</th>
        </tr>
      </thead>
      <tbody>
        {hats.map((hat) => {
          console.log(hats);
          return (
            <tr key={hat.id} className="align-center">
              <td className="w-20 mh-100">{hat.fabric}</td>
              <td className="w-20 mh-100">{hat.style_name}</td>
              <td className="w-20 mh-100">{hat.color}</td>
              <td className="w-40 mh-100">
                <img src={hat.picture_url} alt="" className="w- mh-100" />
              </td>
              <td className="w-20 h-25">{hat.location.name}</td>
              <td>
                <div className="text-center">
                  <button
                    onClick={() => {
                      deleteHat(hat.id);
                    }}
                    className="border-0 rounded text-center"
                  >
                    Delete
                  </button>
                </div>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
