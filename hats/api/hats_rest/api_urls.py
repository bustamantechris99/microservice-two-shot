from django.urls import path
from .views import get_hats_list, get_hat

urlpatterns = [
    path("hats_list", get_hats_list, name='hats_list'),
    path("<int:id>", get_hat, name="get_hat")
]