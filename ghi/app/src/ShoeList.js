import React, { useState, useEffect } from "react";
import "./index.css";

function ShoeList() {
  const [shoes, setShoes] = useState([]);

  const getShoes = async () => {
    const shoesResponse = await fetch("http://localhost:8080/api/shoes/");
    if (shoesResponse.ok) {
      const shoesData = await shoesResponse.json();
      setShoes(shoesData.shoes);
    }
  };

  useEffect(() => {
    getShoes();
  }, [shoes]);

  const deleteShoe = async (id) => {
    const shoeUrl = `http://localhost:8080/api/shoes/${id}`;
    const fetchConfig = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
      setShoes(shoes);
    } else {
      console.log("Not Deleted");
    }
  };
  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Shoes</th>
            <th>Bin</th>
            <th>Image</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map((shoe) => {
            return (
              <tr key={shoe.id}>
                <td>{shoe.model_name}</td>
                <td>{shoe.bin}</td>
                <td>
                  <img alt="" src={shoe.picture_url} />{" "}
                </td>
                <td>
                  <button
                    onClick={() => {
                      deleteShoe(shoe.id);
                    }}
                  >
                    {" "}
                    Delete{" "}
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-center"></div>
    </div>
  );
}

export default ShoeList;
