from django.db import models

class LocationVO(models.Model):
    name = models.CharField(max_length=100)
    href = models.CharField(max_length=100, null=True)



class Hats(models.Model):
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=50, null=True)
    picture_url = models.URLField(max_length=200, null=True)
    location = models.ForeignKey(
      LocationVO,
      related_name = 'hats',
      on_delete=models.CASCADE
    )
