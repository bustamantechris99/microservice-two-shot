import React, { useEffect, useState } from 'react';

function ShoeForm() {
    const [model_name, setName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPictureUrl] = useState('');
    const [bin, setBin] = useState('');
    const [bins, setBins] = useState([]);

    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);

        if (response.ok) {
        const data = await response.json();
        console.log(data)
        setBins(data.bins);
    }
    };

    const binChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    const modelNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const manufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const colorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const pictureChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.model_name = model_name;
        data.manufacturer = manufacturer;
        data.color = color;
        data.picture_url = picture_url;
        data.bin = bin;
        const shoesUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
        }
    };
    const response = await fetch(shoesUrl, fetchConfig);
    if (response.ok) {
        const newShoe = await response.json();
        setName('');
        setManufacturer('');
        setColor('');
        setPictureUrl('');
        setBin('');
        }
    };

    return (
    <div className="row">
    <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
        <h1>Add A New Shoe</h1>
        <form id="create-shoes-form" onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
            <input
                value={model_name}
                placeholder="Model_Name"
                required
                type="text"
                name="model_name"
                id="model_name"
                className="form-control"
                onChange={modelNameChange}
            />
            <label htmlFor="model_name">Model Name</label>
            </div>

            <div className="form-floating mb-3">
            <input
                value={manufacturer}
                placeholder="Manufacturer"
                required
                type="text"
                name="manufacturer"
                id="manufacturer"
                className="form-control"
                onChange={manufacturerChange}
            />
            <label htmlFor="manufacturer"> Manufacturer </label>
            </div>

            <div className="form-floating mb-3">
            <input
                value={color}
                placeholder="ends"
                required
                type="text"
                id="color"
                name="color"
                className="form-control"
                onChange={colorChange}
            />
            <label htmlFor="color">Color</label>
            </div>

            <div className="mb-3">
            <label htmlFor="picture_url"> Picture URL </label>
            <textarea
                value={picture_url}
                required
                id="picture_url"
                name="picture_url"
                className="form-control"
                type="picture_url"
                onChange= {pictureChange}
            ></textarea>
            </div>

            <div className="mb-3">
            <select
                value={bin}
                required
                id="bin"
                className="form-select"
                name="bin"
                onChange={binChange}
            >
                <option value="">
                Choose a bin
                </option>
                {bins.map((bin, index) => {
                    return (
                        <option key={index} value={bin.href}>
                            {bin.closet_name}
                        </option>
                    );
                })}
            </select>
            </div>
            <button className="btn btn-primary">Create</button>
        </form>
        </div>
    </div>
    </div>
);
}


    export default ShoeForm;