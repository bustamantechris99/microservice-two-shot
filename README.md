# Wardrobify

Team:

- Person 1 - Which microservice?
  Christopher Bustamante Hats Microservice
- Person 2 - Which microservice?
  Artemis Liu- Shoes

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

defined api locations/bins to handle the Restful API requests for locations and bin resources in the micrservice.
While the bin and models are defined in models.py to be used in insomnia. The views utilizes the encoder classes to serialize the location and bin objects into json responses.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

I have my hats model with fabric, style name, color, a picture url, and location. The location i used the polling to get the data and once i got the data i created a locationVO with every location from that data. Then i used the model encoders to serialize all my inputs and the location data. I then used Insomnia to check if my inputs were working and after that i used async functions to fetch my data and await my responses. Used that data to be layed out in react and created a form to add hats to my database.
