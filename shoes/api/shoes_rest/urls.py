from django.urls import path
from .views import shoe_list, api_show_shoes

urlpatterns = [
    path('shoes/', shoe_list, name='shoe_list'),
    path('shoes/<int:id>/', api_show_shoes, name='api_show_shoes'),
]