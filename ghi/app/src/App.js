import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import HatForm from "./hats/hatForm";
import HatsList from "./hats/hatsList";
import ShoeList from "./ShoeList";
import ShoeForm from "./ShoeForm";

function App() {
  return (
    <BrowserRouter>
      <Nav className="Navbar" />
      <div>
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path="create" element={<HatForm />} />
            <Route path="list" element={<HatsList />} />
          </Route>
          <Route path="shoes/">
            <Route path="" element={<ShoeList />} />
            <Route path="new" element={<ShoeForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
